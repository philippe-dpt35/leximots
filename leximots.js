"use strict"

const listeLettres = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","à","â","é","è","ê","ë","î","ô","û"];

var motCourant ="";
const motReference = document.getElementById("mot-reference");
const zoneExercice = document.getElementById("zone-exercice");
var ctIdEtiquettes = 0;
var ctLettres = 0;
var tempsAffichage = 2000;
var defautFonte = "script";

function alea(min,max) {
  var nb = min + (max-min+1) * Math.random();
  return Math.floor(nb);
}

function creeListeLettres() {
  let i;
  let newTexte;
  let newDiv;
  for (i = 0; i < listeLettres.length; i++) {
    newDiv = document.createElement("div");
    newDiv.id = "lettre" + i;
    newDiv.className = "lettre";
    newDiv.setAttribute('draggable', 'true');
    newDiv.addEventListener("dragstart", drag);
    newTexte = document.createTextNode(listeLettres[i]);
    newDiv.appendChild(newTexte);
    document.getElementById("zone-lettres").appendChild(newDiv);
  }
}

function creeListeMots() {
  let i;
  const d = document.getElementById("liste-mots");
  for(i=0; i < listeMots.length ; i++) {
    d.length++;
    d.options[d.length-1].text = listeMots[i];
  }
}

function creeEtiquettes() {
  let i;
  let newDiv;
  for (i = 0; i < motCourant.length; i++) {
    newDiv = document.createElement("div");
    newDiv.id = "etiquette" + i;
    newDiv.className = "etiquette";
    newDiv.addEventListener("drop", drop);
    newDiv.addEventListener("dragover", allowDrop);
    newDiv.addEventListener("contextmenu",function(e){e.preventDefault();});
    zoneExercice.appendChild(newDiv);
  }
}

function videZoneExercice() {
  while (zoneExercice.firstChild) {
    zoneExercice.removeChild(zoneExercice.firstChild);
  }
  ctIdEtiquettes = 0;
  ctLettres = 0;
}

function chargeFonte() {
  let i;
  if (( sessionStorage.getItem("fonte") ) && (sessionStorage.getItem("fonte") !== 'undefined')) {
    defautFonte = sessionStorage.getItem("fonte");
  }
  let lettres = document.getElementsByClassName("lettre");
  if ( defautFonte == "cursive" ) {
    for(i = 0; i < lettres.length; i++) {
      lettres[i].classList.add("cursive");
    }
    motReference.classList.add("cursive");
  }
  else {
    for(i = 0; i < lettres.length; i++) {
      lettres[i].classList.remove("cursive");
    }
    motReference.classList.remove("cursive");
  }
}

function efface(ev) {
  const objetCible = document.getElementById(ev.target.id);
  if (event.button == 2) {
    let infosObjet;
    infosObjet = ev.target.id;
    objetCible.parentNode.removeChild(objetCible);
    ctLettres-= 1;
  }
}

function afficheMot() {
  const liste = document.getElementById("liste-mots");
  document.getElementById("btrevoir").disabled = true;
  motCourant = liste.options[liste.selectedIndex].value;
  if ( defautFonte =="cursive" ) {
    motReference.classList.add("cursive");
  }
  motReference.textContent = motCourant;
  videZoneExercice();
  creeEtiquettes();
  if (( sessionStorage.getItem("temps") ) && (sessionStorage.getItem("temps") !== 'undefined')) {
    tempsAffichage = sessionStorage.getItem("temps");
  }
  setTimeout(cacheMot, tempsAffichage);
  document.getElementById("btrevoir").disabled = false;
}

function cacheMot() {
  let i;
  let cache ="";
  for (i = 0; i < motCourant.length; i++) {
    cache = cache + "X";
  }
  motReference.classList.remove("cursive");
  motReference.textContent = cache;
}

function afficheMotAleatoire() {
  motCourant = listeMots[alea(0,listeMots.length)];
  if ( defautFonte =="cursive" ) {
    motReference.classList.add("cursive");
  }
  motReference.textContent = motCourant;
  videZoneExercice();
  creeEtiquettes();
  if (( sessionStorage.getItem("temps") ) && (sessionStorage.getItem("temps") !== 'undefined')) {
    tempsAffichage = sessionStorage.getItem("temps");
  }
  setTimeout(cacheMot, tempsAffichage);
  document.getElementById("btrevoir").disabled = false;
}

function revoirMot() {
  if ( defautFonte =="cursive" ) {
    motReference.classList.add("cursive");
  }
  motReference.textContent = motCourant;
  setTimeout(cacheMot, tempsAffichage);
}

function drag(ev) {
  let infosObjet;
  infosObjet = ev.target.id;
  ev.dataTransfer.setData("text", infosObjet);
}

function allowDrop(ev) {
  ev.preventDefault();
}

function drop(ev) {
  let nodeCopy;
  ev.preventDefault();
  const idEtiquette = ev.dataTransfer.getData("text");
  const cible = document.getElementById(ev.target.id);
  if ( cible.className !== "etiquette") {return};
  nodeCopy = document.getElementById(idEtiquette).cloneNode(true);
  nodeCopy.id = idEtiquette + "_" + ctIdEtiquettes;
  ctIdEtiquettes += 1;
  nodeCopy.addEventListener("mouseup", efface);
  ev.target.appendChild(nodeCopy);
  ctLettres += 1;
  if ( ctLettres == motCourant.length) {
    setTimeout(verification, 250);
  }
}

function verification() {
  let i;
  let etiquette;
  let motSaisi = "";
  for (i = 0; i < ctLettres; i++) {
    etiquette = document.getElementById("etiquette" + i);
    motSaisi = motSaisi + etiquette.lastChild.textContent;
  }
  if ( motSaisi == motCourant ) {
    showDialog('Bravo !',0.5,'img/happy-tux.png', 89, 91, 'left');
  }
}

function lanceOptions() {
  document.getElementById("ecran-blanc").className = "visible";
  document.getElementById("options").className = "visible";
}

function annuleOptions() {
  document.getElementById("ecran-blanc").className = "invisible";
  document.getElementById("options").className = "invisible";
}

function valideOptions() {
  let i;
  const btTemps = document.getElementsByName('temps');
  const btFonte = document.getElementsByName('fonte');
  let valeurTemps = "";
  let valeurFonte = ""
  for(i = 0; i < btTemps.length; i++){
    if(btTemps[i].checked){
      valeurTemps = btTemps[i].value;
    }
  }
  for(i = 0; i < btFonte.length; i++){
    if(btFonte[i].checked){
      valeurFonte = btFonte[i].value;
    }
  }
  switch(valeurTemps) {
    case '1s':
      sessionStorage.setItem("temps",1000);
      break;
    case '2s':
      sessionStorage.setItem("temps",2000);
      break;
    case '3s':
      sessionStorage.setItem("temps",3000);
      break;
    case '4s':
      sessionStorage.setItem("temps",4000);
      break;
    case '5s':
      sessionStorage.setItem("temps",5000);
      break;
    case '10s':
      sessionStorage.setItem("temps",10000);
      break;
    case 'infini':
      sessionStorage.setItem("temps",360000);
      break;
  }
   if (valeurFonte == "script") {
    sessionStorage.setItem("fonte","script");
    }
    else {
      sessionStorage.setItem("fonte","cursive");
  }
}

creeListeLettres();
creeListeMots();
chargeFonte();
