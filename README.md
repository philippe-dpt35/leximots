Adaptation du logiciel "Leximots" inclus dans le lanceur d'applications Clicmenu de PragmaTICE.

Reconstituer des mots en déplaçant des étiquettes lettres.

L'application peut être testée en ligne [ici](https://primtux.fr/applications/leximots/index.html)
